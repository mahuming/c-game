/*【例5-4】求100以内的全部素数，每行输出10个。素数就是只能被1和自身整除的正整数，1不是素数，2是素数。要求定义和调用函数prime(m)判断m是否为素数，当m为素数时返回1，否则返回0。*/

/* 使用函数求100以内的全部素数 */
#include <stdio.h>
#include <math.h>					/* 调用求平方根函数，需要包含数学库 */
int main(void)
{
    int count, m;   
	int prime(int m);				/* 函数声明 */

    count = 0;						/* count记录素数的个数，用于控制输出格式 */
    for(m = 2; m <= 100; m++){
        if(prime(m)!=0){		      	/* 调用prime(m)判断m是否为素数 */
             printf("%6d", m);     	/* 输出m */
             count++;            	/* 累加已经输出的素数个数 */
             if (count %10 == 0)  printf("\n");   /* 如果count是10的倍数，换行 */
        }
    }
    printf("\n");
}
/* 定义判断素数的函数，如果m是素数则返回1（"真"）；否则返回0（"假"）*/
int prime(int m)
{
    int i, n;

    if(m == 1) return 0;   	    /* 1不是素数，返回0 */
    //n = sqrt(m);
    for( i = 2; i <= m-1; i++)     
       if (m % i == 0){   		/* 如果m不是素数 */
           return 0;			/* 返回0 */
       }

    return 1;					/* m是素数，返回1 */
}
