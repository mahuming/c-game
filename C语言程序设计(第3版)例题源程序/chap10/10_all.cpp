#include<stdio.h>

#define FLAG 1

#define PAI 3.1415 //宏是代替。 

#define  MAX(a, b)  a > b ? a: b
#define  SQR(x)  x * x

#include "myheader.h"  /* 包含自定义头文件。 一般用双引号"" , 系统提供的一般用<> */ 

/*【例10-6】简单的带参数的宏定义。*/
int ex_10_6_macro(void)
{
	 int  radius,  x , y;

	 scanf ("%d" , &radius) ;
	 printf("square is %f\n" , PAI * radius * radius) ;
	 printf("circum is %f\n" , 2* PAI * radius) ;  //名字比数字要容易识别。 如果没有宏，每个地方都用数字会非常繁琐。

	 printf("square2 is %lf\n" , PAI2 * radius * radius) ;
	 printf("circum2 is %lf\n" , 2* PAI2 * radius) ;  //名字比数字要容易识别。 如果没有宏，每个地方都用数字会非常繁琐。
	 
	 scanf ("%d%d" , &x, &y) ;
	 x = MAX (x, y);			/* 引用宏定义 */
	 y = SQR(x); 				/* 引用宏定义 */
	 printf("%d  %d\n" , x, y) ;
	 
	 #if FLAG
	 printf("1") ;
	 #else
	 printf("2") ;
	 #endif
	
	 return 0;
}

/*【例10-2】用递归函数实现求n！*/
double  fact(int n)   				/*  函数定义  */
{ 
    double result;
    if (n==1 || n==0) 				/*  递归出口  */
        result = 1;
    else 
        result = n * fact(n-1);		/*	函数递归调用 */

    return result;
}

int main(void)
{
	int choice, i;

	for (i = 1; i <= 3; i++) {		/************for 的循环体语句开始*****************/
		printf("Enter choice: ");             /* 输入提示 */
		scanf("%d", &choice);                 /* 开关打向哪个函数 */
		rewind(stdin); //本函数与本章无关请忽视。 （rewind函数是把指定流的读写指针重新指向开头。比如scanf或getchar）
		
		double a,b,c;
		switch (choice) {
			case 102:  a = fact(1);		/*【例10-2】用递归函数实现求n！*/
					   b = fact(2);
					   c = fact(3);
					   break;          
			case 106:  ex_10_6_macro(); /*【例10-6】简单的带参数的宏定义。*/
					   break;      /* 【例8-2】取地址运算和间接访问运算示例  */
		}
	}                         		/*************for 的循环体语句结束*****************/
	
	return 0;
}