#include<stdio.h>

struct student{       /*学生信息结构定义*/
  int num;            /* 学号 */
  char name[10];      /* 姓名 */
  int math, english, computer;   /* 三门课程成绩 */
  double average;          /* 个人平均分 */
}; 

/* 【例9-1】假设学生的基本信息包括学号、姓名、三门课程成绩以及个人平均成绩。
输入3个学生的成绩信息，计算并输出平均分最高的学生信息。 */
int ex_91_average_highest(void)
{    
	struct student s1,max;         /* 定义结构变量 */
	for(int i = 1; i <= 3; i++){
		scanf("%d%s%d%d%d",&s1.num,s1.name,&s1.math,&s1.english,&s1.computer);
		s1.average=(s1.math + s1.english + s1.computer) / 3.0;
		if(i == 1)  
			max = s1;
		if(max.average < s1.average)	 
			max = s1;
	}
	printf("num:%d, name:%s, average:%.2lf\n", max.num, max.name, max.average);

	return 0;
}

int main(void)
{
	int choice, i;

	for (i = 1; i <= 3; i++) {		/************for 的循环体语句开始*****************/
		printf("Enter choice: ");             /* 输入提示 */
		scanf("%d", &choice);                 /* 开关打向哪个函数 */
		rewind(stdin); //本函数与本章无关请忽视。 （rewind函数是把指定流的读写指针重新指向开头。比如scanf或getchar）
											  
		switch (choice) {
			case 91:   ex_91_average_highest(); 
					   break;           /* 【例8-1】利用指针模拟密码开锁游戏 */
			case 92:   int ex_92_sort_degree(void);
					   ex_92_sort_degree(); 
					   break;      /* 【例8-2】取地址运算和间接访问运算示例  */
			case 93:   int ex_93_update_score(void);
					   ex_93_update_score(); 
					   break;      		/*  通过函数调用来交换变量值的示例程序 */
		}
	}                         		/*************for 的循环体语句结束*****************/
	
	return 0;
}

/* 【例9-3】输入n(n<50)个学生的成绩信息，再输入一个学生的学号、课程以及成绩，在自定义函数中修改该学生指定课程的成绩。*/
 /* 修改学生成绩，结构指针作为函数参数 */

int update_score(struct student *p, int n, int num, int course, int score); /*函数声明*/
int ex_93_update_score(void)
{    
  int i, pos, n, num, course, score;
  struct student students[50];   /* 定义结构数组 */

  /* 输入n个学生信息 */
  printf("Input n: ");
  scanf("%d", &n);
  for(i = 0; i < n; i++){
    printf("Input the info of No.%d:\n", i+1);
    printf("number:");
    scanf("%d", &students[i].num);
    printf("name:");
    scanf("%s", students[i].name); 
    printf("math score:");
    scanf("%d", &students[i].math); 
    printf("english score:");
    scanf("%d", &students[i].english); 
    printf("computer score:");
    scanf("%d", &students[i].computer);
  }

  /* 输入待修改学生信息 */
  printf("Input the number of updated student: ");
  scanf("%d", &num);
  printf("Choice the course: 1.math 2.english 3.computer: ");
  scanf("%d", &course);
  printf("Input the new score: ");
  scanf("%d", &score);

  /*调用函数，修改学生成绩*/
  pos = update_score(students, n, num, course, score);

  /*输出修改后的学生信息*/
  if(pos == -1)
    printf("Not found!\n");
  else
  {  printf("After update:\n");
     printf("num\t math\t english\t computer\n");
     printf("%d\t %d\t %d\t %d\n", students[pos].num, students[pos].math, students[pos].english, students[pos].computer);
  }

  return 0;
}

/* 自定义函数，修改学生成绩 */
int update_score(struct student *p, int n, int num, int course, int score)
{ 
  int i,pos;
  for(i = 0; i < n; i++, p++)  /* 按学号查找 */
    if(p->num == num)
      break;
  if(i < n) /* 找到，修改成绩 */
  {
    switch(course){
      case 1: p->math = score; break;
      case 2: p->english = score; break;
      case 3: p->computer = score; break;
	}
    pos = i;  /* 学生位置 */
  }
  else   /* 无此学号 */
    pos = -1;
  return pos;
}

/* 【例9-2】输入3个学生的成绩信息，按照学生的个人平均分从高到低输出他们的信息。 */
int ex_92_sort_degree(void)
{    
  int i, j, n, index;
  struct student students[50], temp;   /* 定义结构数组 */
  
  /* 输入 */
  n = 3;
  for(i = 0; i <n; i++){
    printf("number:");
    scanf("%d", &students[i].num);
    printf("name:");
    scanf("%s", students[i].name); 
    printf("math score:");
    scanf("%d", &students[i].math); 
    printf("english score:");
    scanf("%d", &students[i].english); 
    printf("computer score:");
    scanf("%d", &students[i].computer);
    //students[i].average = count_average(students[i]);   /* 计算平均分 */
	students[i].average = (students[i].math + students[i].english + students[i].computer) / 3.0;   /* 计算平均分 */
  }

  /* 结构数组排序，选择排序法 */
  for( i = 0; i < n-1; i++ ){
    index = i;
    for (j = i+1; j <n; j++ )	
      if (students[j].average > students[index].average)   /* 比较平均分*/
          index = j;
    temp = students[index];		/* 交换数组元素 */
    students[index] = students[i];
    students[i] = temp;
  }

  /* 输出排序后的信息 */
  printf("num\t name\t average\n");
  for (i = 0; i < n; i++ ) 
    printf("%d\t %s\t %.2lf\n", students[i].num, students[i].name, students[i].average);

  return 0;
}