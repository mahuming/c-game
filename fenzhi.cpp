/////////////////////第三章/////////////////////

# include <stdio.h>

/* 【例3-1】简单的猜数游戏。输入你所猜的整数（假定1~100内），与计算机产生的被猜数比较，若相等，显示猜中；若不等，显示与被猜数的大小关系。*/
int ex_3_1_guess_number(void)
{
	int  mynumber = 38;                /* 计算机指定被猜的数 */
	int  yournumber;

	printf("Input your number: ");     /* 提示输入你所猜的整数 */
	scanf("%d", &yournumber);
	if (yournumber == mynumber)         /* 若相等，显示猜中 */
		printf("Ok! you are right!\n");
	else                               /* 若不等，比较大小 */
		if (yournumber > mynumber)
			printf("Sorry! your number is bigger than my number!\n");
		else
			printf("Sorry! your number is smaller than my number!\n");

	return 0;
}

/* 【例3-2】输入一个整数，判断该数是奇数还是偶数。*/
int ex_3_2_odd_even(void)
{
	int number;

	printf("Enter a number: ");  /* 输入提示 */
	scanf("%d", &number);   	 /* 输入一个数 */
	if (number % 2 == 0) {         /* 若number除以2的余数是0，则为偶数 */
		printf("Tne number is even. \n");
	}
	else {                        /* 若number除以2的余数不是0，则为奇数 */
		printf("Tne number is odd. \n");
	}

	return 0;
}

/* 【例3-3】输入一个正整数n，再输入n个学生的成绩，计算平均成绩，并统计不及格学生的人数。 */
//int ex_3_3_average_grade(void)
//{
//	int count, i, n;                   /* count 记录不及格学生的人数 */
//	double grade, total;               /* grade 存放输入的成绩, total保存成绩之和 */

//	printf("Enter n: ");               /* 提示输入学生人数n */
//	scanf("%d", &n);
//	total = 0;
//	count = 0;
//	for (i = 1; i <= n; i++) {
//		printf("Enter grade #%d: ", i);/* 提示输入第i个成绩 */
//		scanf("%lf", &grade);         /* 输入第i个成绩 */
//		total = total + grade;         /* 累加成绩 */
//		if (grade < 60) {                /* 统计不及格学生的人数 */
//			count++;
//		}
//	}
//	printf("Grade average = %.2f\n", total / n);
//	printf("Number of failures = %d\n", count);

//	return 0;
//}

/* 【例3-4】继续讨论例2-4中提出的分段计算水费的问题 */
int ex_3_4_piecewise_function(void)
{
	double x, y;                /* 定义两个双精度浮点型变量 */

	printf("Enter x:");         /* 输入提示 */
	scanf("%lf", &x);           /* 输入 double 型数据用 %lf */
	if (x < 0) {
		y = 0;                  /* 满足x＜0 */
	}
	else if (x <= 15) {
		y = 4 * x / 3;          /* 不满足x＜0，但满足x≤15，即满足0≤x≤15 */
	}
	else {
		y = 2.5 * x - 10.5;     /* 既不满足x＜0，也不满足x≤15，即满足x>15 */
	}
	printf("f(%.2f) = %.2f\n", x, y);

	return 0;
}


/* 【例3-5】求解简单的四则运算表达式。输入一个形式如"操作数 运算符 操作数"的四则运算表达式，输出运算结果。 */
int ex_3_5_4opreatior(void)
{
	double value1, value2;
	char op;

	printf("Type in an expression: ");                 /* 提示输入一个表达式 */
	scanf("%lf%c%lf", &value1, &op, &value2);    /* 输入表达式 */

	if (op == '+')                                /* 判断运算符是否为 '+' */
		printf("=%.2f\n", value1 + value2);            /* 对操作数做加法操作*/
	else if (op == '-')                           /* 否则判断运算符是否为 '-' */
		printf("=%.2f\n", value1 - value2);
	else if (op == '*')                           /* 否则判断运算符是否为 '-' */
		printf("=%.2f\n", value1 * value2);
	else if (op == '/')                           /* 否则判断运算符是否为 '-' */
		printf("=%.2f\n", value1 / value2);
	else                                               /* 运算符输入错误 */
		printf("Unknown operator\n");

	return 0;
}

/*【例3-7】输入10个字符，统计其中英文字母、数字字符和其他字符的个数 */
//int ex_3_7_count_char_int(void)
//{
//	int digit, i, letter, other;               /* 定义三个变量分别存放统计结果 */
//	char ch;                                   /* 定义一个字符变量ch */

//	digit = letter = other = 0;                /* 置存放统计结果的三个变量的初值为零 */
//	printf("Enter 10 characters: ");           /* 输入提示 */
//	for (i = 1; i <= 10; i++) {                  /* 循环执行了10次 */
//		ch = getchar();                        /* 从键盘输入一个字符，赋值给变量 ch */
//		if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'))
//			letter++;                         /* 如果ch是英文字母，累加letter */
//		else if (ch >= '0' && ch <= '9')
//			digit++;                          /* 如果ch是数字字符，累加digit */
//		else
//			other++;                          /* ch是除字母、数字字符以外的其他字符，累加other */
//	}
//	printf("letter=%d, digit=%d, other=%d\n", letter, digit, other);

//	return 0;
//}

/* 【例3-8】查询自动售货机中商品的价格。假设自动售货机出售4种商品：薯片(crisps)、爆米花(popcorn)、巧克力(chocolate)和可乐(cola)，售价分别是每份3.0、2.5、4.0和3.5元。在屏幕上显示以下菜单（编号和选项），用户可以连续查询商品的价格，当查询次数超过5次时，自动退出查询；不到5次时，用户可以选择退出。当用户输入编号1～4，显示相应商品的价格（保留1位小数）；输入0，退出查询；输入其他编号，显示价格为0。 */
int ex_3_8_commodity_price(void)
{
	int choice, i;
	double price;

	for (i = 1; i <= 5; i++) {                /* for 的循环体语句开始 */
											  /* 以下5行显示菜单 */
		printf("[1] Select crisps \n");       /* 查询薯片价格 */
		printf("[2] Select popcorn \n");      /* 查询爆米花价格 */
		printf("[3] Select chocolate \n");    /* 查询巧克力价格 */
		printf("[4] Select cola \n");         /* 查询可乐价格 */
		printf("[0] exit \n");                /* 退出查询 */

		printf("Enter choice: ");             /* 输入提示 */
		scanf("%d", &choice);                 /* 接受用户输入的编号 */

											  /* 如果输入0，提前结束 for 循环 */
		if (choice == 0)
			break;                            /* 此处用 break 跳出 for 循环 */
											  /* 根据输入的编号，将相应的价格赋给price */
		switch (choice) {
		case 1: price = 3.0; break;       /* 用break跳出switch语句，下同 */
		case 2: price = 2.5; break;
		case 3: price = 4.0; break;
		case 4: price = 3.5; break;
		default: price = 0.0; break;
		}
		/* 输出商品的价格 */
		printf("price = %0.1f\n", price);
	}                                        /* for 的循环体语句结束 */

	printf("Thanks \n");                     /* 结束查询，谢谢用户使用 */

	return 0;
}

/* 【例3-9】求解简单表达式。输入一个形式如"操作数 运算符 操作数"的四则运算表达式，输出运算结果，要求使用switch语句编写。 */
int ex_3_9_4opreatior_switch(void)
{
	double value1, value2;
	char op;

	printf("Type in an expression: ");    /* 提示输入一个表达式 */
	scanf("%lf%c%lf", &value1, &op, &value2);
	switch (op) {
	case '+':
		printf("=%.2f\n", value1 + value2);
		break;
	case '-':
		printf("=%.2f\n", value1 - value2);
		break;
	case '*':
		printf("=%.2f\n", value1 * value2);
		break;
	case '/':
		printf("=%.2f\n", value1 / value2);
		break;
	default:
		printf("Unknown operator\n");
		break;
	}

	return 0;
}

/* 【例3-10】输入10个字符，分别统计出其中空格或回车、数字字符和其他字符的个数。 */
//int ex_3_10_count_char_int_switch(void)
//{
//	int blank, digit, i, other;     /* 定义三个变量分别存放统计结果 */
//	char ch;

//	blank = digit = other = 0;      /* 置存放统计结果的三个变量的初值为零 */
//	printf("Enter 10 characters: ");/* 输入提示 */
//	for (i = 1; i <= 10; i++) {       /* 循环执行了10次 */
//		ch = getchar();             /* 输入一个字符 */
									/* 在switch语句中灵活应用break */
//		switch (ch) {
//		case ' ':               /* 语句段为空，请注意空格的表示 */
//		case '\n':
//			blank++;           /* 两个常量表达式 ' ' 和 '\n' 共用该语句段 */
//			break;              /* 跳出switch语句 */
//		case '0': case '1': case '2': case '3': case '4':
//		case '5': case '6': case '7': case '8': case '9':
//			digit++;           /* 10个常量表达式 '0' ～ '9' 共用该语句段*/
//			break;              /* 跳出switch语句 */
//		default:
//			other++;           /* 累加其他字符 */
//			break;              /* 跳出switch语句 */
//		}
//	}
//	printf("blank=%d, digit=%d, other=%d\n", blank, digit, other);

//	return 0;
//}


/* 【例3-11】求解简单表达式。输入一个形式如"操作数 运算符 操作数"的四则运算表达式，输出运算结果，要求对除数为0的情况作特别处理。 */
int ex_3_11_4opreatior_divisior0(void)
{
	double value1, value2;

	char op;

	printf("Type in an expression: ");    /* 提示输入一个表达式 */
	scanf("%lf%c%lf", &value1, &op, &value2);
	if (op == '+')
		printf("=%.2f\n", value1 + value2);
	else if (op == '-')
		printf("=%.2f\n", value1 - value2);
	else if (op == '*')
		printf("=%.2f\n", value1 * value2);
	else if (op == '/')
		if (value2 != 0)                  /* 嵌套的if，判断除数是否为0 */
			printf("=%.2f\n", value1 / value2);
		else
			printf("Divisor can not be 0!\n");
	else
		printf("Unknown operator!\n");

	return 0;
}

int lianxi_odd_even(void)
{


	return 0;
}

int lianxi_p(void)
{


	return 0;
}

int main(void)
{
	int choice, i;

	for (i = 1; i <= 10; i++) {                /* for 的循环体语句开始 */
		printf("Enter choice: ");             /* 输入提示 */
		scanf("%d", &choice);                 /* 接受用户输入的编号 */
											  /* 根据输入的编号，将相应的价格赋给price */
		switch (choice) {
			case 31: ex_3_1_guess_number(); break;       /* 用break跳出switch语句，下同 */
			case 32: ex_3_2_odd_even(); break;       /* 用break跳出switch语句，下同 */
			case 321: lianxi_odd_even(); break;
			//case 33: ex_3_3_average_grade(); break;
			case 34: ex_3_4_piecewise_function(); break;
			case 341: lianxi_p(); break;

			case 35: ex_3_5_4opreatior(); break;
			//case 37: ex_3_7_count_char_int(); break;
			case 38: ex_3_8_commodity_price(); break;
			case 39: ex_3_9_4opreatior_switch(); break;
			//case 310: ex_3_10_count_char_int_switch(); break;       /* 用break跳出switch语句，下同 */
			case 311: ex_3_11_4opreatior_divisior0(); break;       /* 用break跳出switch语句，下同 */

			default: printf("err"); break;
		}
		/* 输出商品的价格 */
		printf("choice = %d end\n", choice);
	}                                        /* for 的循环体语句结束 */

	printf("Thanks \n");                     /* 结束查询，谢谢用户使用 */

	return 0;
}

//分支练习1： 模仿case 32对应的ex_3_2_odd_even()，自己写新函数实现同样功能。
//然后在上面switch中模仿增加新的case，调用测试他。结果正确后，提交至码云gitee。
//！！！注意： 老师已添加了，case 321: lianxi_odd_even(), 你只用在它里面添加功能代码后，f10查看结果正确即可） 

//分支练习2：  同样的操作，写出case 34对应的ex_3_4_piecewise_function()。 结果正确后，提交至码云gitee。
